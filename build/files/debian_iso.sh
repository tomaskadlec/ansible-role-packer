#!/bin/bash
# Updates ISO image boot menu - single preconfigured option
# Script create $ISO_FILE.packer ISO file (e.g. mini.iso.packer)

# $1 - build dir
BUILD_DIR="$1"

# $2 - ISO filename
ISO_FILE="$2"

# $3 - kernel command line arguments
KERNEL_ARGUMENTS="$3"

# temporay unpacked ISO dir
ISO_DIR="iso"

cd "$BUILD_DIR"

mkdir -p "$ISO_DIR"

7z -o"$ISO_DIR/" x "$ISO_FILE"

cat << --MENU-- > "$ISO_DIR/isolinux.cfg"
path
menu hshift 7
menu width 61
menu title Debian GNU/Linux installer boot menu
include stdmenu.cfg
default vesamenu.c32
prompt 0
timeout 0

label install
    menu label Install
    kernel linux
    append vga=788 initrd=initrd.gz $KERNEL_ARGUMENTS
--MENU--

genisoimage -o "${ISO_FILE}.packer.iso" -b isolinux.bin -c boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table "$ISO_DIR"

rm -rf "$ISO_DIR"
