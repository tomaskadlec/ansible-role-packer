packer/build
============

.. _`Ansible`: https://ansible.com
.. _`Packer`: https://www.packer.io/
.. _`README`: ../README.rst

This role is used to build images.

Architecture
------------

.. _`connection variables`: http://docs.ansible.com/ansible/intro_inventory.html#hosts-and-groups

The role expects that images are defined as hosts, i.e.:

* Image is defined as host in inventory file and is member of ``images`` group.
* Each host in ``images`` group has configured ``ansible_host`` (and the rest of
  required `connection variables`_) that resolves to an appropriate
  ``buildhost``.

Configuration
-------------

build_base: (required)
    Path to build directory. Defaults to ``build``.

    Each host has its own build directory ``{{build_base}}/{{inventory_hostname}}``.
    Each builder within a particular host has a directory named by its type
    (e.g. ``qemu``) in this directory.

    Please refer to a full example later in the document.

Each host requires configuration that has attributes:

installer (required)
    Data structure describing installer.

    Please refer to the section `Installer <#installer>`_ for further
    information.

disk_size (optional)
    VM disk size. Defaults to 10 GiB.

builders (required, list)
    A list of builders. ``qemu`` and ``virtualbox-iso`` are supported currently.

    Please refer to section `Builder <#builder>` for further information.

provisioners (optional, list)
    A list of provisioners. If defined they will run subsequently on each image
    produced by defined builders.

    Please refer to section `Provisioner <#provisioner>` for further
    information.

postprocessors (optional, list)
    A list of postprocessors. If defined they will run subsequently on each image
    produced by defined builders and provisoned by provisioners.

    Please refer to section `Provisioner <#provisioner>` for further
    information.

It is possible to provide additional attributes used for particular image (e.g.
``hostname`` that will be used in a template then).

.. _`defaults/main.yml`: defaults/main.yml
.. |defaults/main.yml| replace:: ``defaults/main.yml``

Please refer to |defaults/main.yml|_ for an example configuration (Debian
Testing).

Installer
~~~~~~~~~

Installer is just a description how to obtain an ISO file (or other files that
will be used to create an ISO file at the end). Following excerpt is a complete
reference. ::

    # name of the ISO file in build directory, required
    iso: mini.iso
    # kernel or installer options, optional, used with script only
    options: 'locale=en_US.UTF-8 keymap=us biosdevname=0 net.ifnames=0'
    # script to create/modify ISO file, optional
    # $1 - path to build directory
    # $2 - value of iso attribute
    # $3 - value of options attribute (kernel options typically)
    script: debian_iso.sh
    files:
       -
            # URL to download file from
            url: http://{{debian.mirror}}/debian/dists/stable/main/installer-amd64/current/images/netboot/mini.iso
            # filename in build dir
            file: mini.iso
            # checksum information - it is parsed from a file with checksums
            checksum:
                url: http://{{mirror}}/debian/dists/stable/main/installer-amd64/current/images/SHA256SUMS
                alg: sha256
                type: listing
                pattern: '[.]/netboot/mini.iso'
                delimiter: ' '
                field: 1

Builder
~~~~~~~

Packer_ is able to use different builder(s) - based on virtualization platform
used. The role works with ``qemu`` and ``virtualbox-iso`` builders but it should
work with the others as well (if correct template is provided to Packer_).

Each image may define multiple builders. Image is built by all defined builders
then. Following excerpt is a complete reference for builder configuration. ::

    # builder type: qemu|virtualbox-iso
    type: qemu
    # configuration templates, attribute names are mandatory!
    # no other files are supported!
    templates:
        # packer template
        packer_json:
            # destination in build dir
            dest: packer.json
            # source, it is processed by Ansible template module
            template: debian_testing/packer.json.qemu.j2
        # configuration or script for unattended installation
        unattended_install:
            # destination in build dir
            dest: preseed.cfg
            # source, it is processed by Ansible template module
            template: debian_testing/preseed.cfg.qemu.j2

Provisioner
~~~~~~~~~~~

Provisioners are used to configure the new VM after installer finished its job.
This role works with Ansible_ as provisioner but another ones might be working
as well.

Even before Packer_ is launched Ansible_ (outer one) prepares an environment in
build directory to run provisioners. That means:

#. Ansible_ directory structure is created in build directory
#. Current host variables (except those starting with ``ansible_``) are dumped
   to ``host_vars/{{inventory_hostname}}.yml`` and provisioner might load them.
#. Files and directories required by each provisioner are copied to build
   directory.

   ``synchronize``
       A list of files and directories to synchronize. Each is represented as
       a dict with attributes:

       ``src``, required
           source on controller machine

       ``dest``, optional
           destination in build directory, if omitted ``src`` is used

``provisioner``, required
    A provisioner definition as in Packer_ template file (will be converted to
    JSON format).

Full example follows: ::

    synchronize:
        -   {src: roles/debian-apt/, dest: roles/debian-apt}
        -   {src: roles/packer/build/files/provision/install-software-apt.yml, dest: provision-install-software-apt.yml}
        -   {src: roles/packer/build/files/provision/users.yml, dest: provision-users.yml}
    provisioner:
        type: ansible
        playbook_file: "{{build_dir}}/provision.yml"
        host_alias: "{{inventory_hostname}}"
        extra_arguments: ['-v']

Postprocessor
~~~~~~~~~~~~~

Configuration of postprocessors is similar to configuration of provisioners. The
differences are:

#. Files only can be copied to build directory and these files are treated as
   templates (processed by `template module`_).
#. Custom attributes may be provided and used in templates.

Take following excerpt as a full reference: ::

    # list of templates
    templates:
        -   {template: 'Vagrantfile.j2', dest: 'Vagrantfile'}
    # postprocessor definition for Packer
    postprocessor:
        type: vagrant
        keep_input_artifact: yes
        output: "{{build_dir}}/{{hostname}}.box"
        vagrantfile_template: "{{build_dir}}/Vagrantfile"
    # custom attribute
    memory: 2048


Usage
-----

Include the role in your playbook: ::

	-   name: "Build images"
    	hosts: [images]
	    roles:
    	    -   role: packer/build

Run the playbook. You may limit images created by using ``--limit`` option with
image name or names.

The result is a directory structure similar to the that follows: ::

        build/debian_testing
        |-- qemu
        |   |-- debian-testing-2017-03-08
        |   |   `-- image.qcow2
        |   |-- debian-testing-2017-03-08.box
        |   |-- files
        |   |-- host_vars
        |   |   `-- debian_testing.yml
        |   |-- mini.iso
        |   |-- mini.iso.packer.iso
        |   |-- packer.json
        |   |-- packer.log
        |   |-- preseed.cfg
        |   |-- provision-install-software-apt.yml
        |   |-- provision-users.yml
        |   |-- provision.yml
        |   |-- roles
        |   |   `-- debian-apt
        |   |       |-- CHANGELOG.rst
        |   |       |-- defaults
        |   |       |   `-- main.yml
        |   |       |-- LICENSE
        |   |       |-- README.rst
        |   |       `-- tasks
        |   |           |-- main.yml
        |   |           `-- tasks.yml
        |   |-- templates
        |   `-- Vagrantfile
        `-- virtualbox-iso
            |-- debian-testing-2017-03-08
            |   |-- image-disk1.vmdk
            |   `-- image.ovf
            |-- debian-testing-2017-03-08.box
            ... the rest is the same as for qemu

Extras
------

.. _`../examples/host_vars/debian_testing.yml`: ../examples/host_vars/debian_testing.yml
.. |../examples/host_vars/debian_testing.yml| replace:: ``../examples/host_vars/debian_testing.yml``
.. _`files/debian_iso.sh`: files/debian_iso.sh
.. |files/debian_iso.sh| replace:: ``files/debian_iso.sh``
.. _`templates/debian_testing`: templates/debian_testing
.. |templates/debian_testing| replace:: ``templates/debian_testing``

The role provides configuration and templates for building Debian Testing
images. Refer to following files:

* |defaults/main.yml|_ - ``{{debian_installer}}`` configuration,
* |../examples/host_vars/debian_testing.yml|_ - image configuration (example),
* |templates/debian_testing|_ - templates for Packer_ and Preseed,
* |files/debian_iso.sh|_ - a script used to adjust ISO bootloader configuration.


.. _`../examples/host_vars/gentoo_stage4.yml`: ../examples/host_vars/gentoo_stage4.yml
.. |../examples/host_vars/gentoo_stage4.yml| replace:: ``../examples/host_vars/gentoo_stage4.yml``
.. _`templates/gentoo_stage4`: templates/gentoo_stage4
.. |templates/gentoo_stage4| replace:: ``templates/gentoo_stage4``

The role provides configuration and templates for building Gentoo Stage4
images. Refer to:

* |defaults/main.yml|_ - base configuration,
* |../examples/host_vars/gentoo_stage4.yml|_ - image configuration (example),
* |templates/gentoo_stage4|_ - templates for Packer_ and install script.

.. vim: spelllang=en spell textwidth=80 fo-=l:
.. reformat paragraphs to 80 characters: select using v then hit gq
