packer/requirements
===================

.. _`Ansible`: https://ansible.com
.. _`libvirt-daemon`: https://packages.debian.org/search?keywords=libvirt-daemon&searchon=names&suite=all&section=all
.. _`Packer`: https://www.packer.io/
.. _`qemu-kvm`: https://packages.debian.org/search?keywords=qemu-kvm&searchon=names&suite=all&section=all
.. _`README`: ../README.rst
.. _`VirtualBox`: https://www.virtualbox.org/
.. _`VirtualBox page`: https://wiki.debian.org/VirtualBox

This role is used to install requirements on ``buildhost`` (one or more depends
on your project setup) to actually start building images. It installs `qemu-kvm`_
and `libvirt-daemon`_ by default. However, it is possible to install `VirtualBox`_
also (for installation steps refer to `VirtualBox page`_ on Debian wiki).

Configuration
-------------

apt.software (required, list)
    A list of packages (package names) to install in the latest version
    available.

apt.groups (required, string - comma-separated values)
    A comma-separated list of groups that should be added to ``buildhost`` users
    that is used to create images.

apt.virtualbox (optional)
    If you request installation of VirtualBox_, provide package name to install
    here, e.g. ``virtualbox-5.1`` (Debian Stretch).

build_user_id (required)
    Username of the user that will be used to create images.

packer.url (required)
    URL Packer_ can be downloaded from.

packer.dest (required)
    Directory Packer_ is downloaded to. It is advisable to have this directory
    in ``$PATH``. Defaults to ``/usr/local/bin``.

packer.owner, packer.group
    Packer_ binary ownner and group. Defaults to ``root``, ``root``.

Usage
-----

Include the role in your playbook. ::

    -   name: "Install requirements on buildhost"
        hosts: [buildhost]
        pre_tasks:
            -   set_fact:
                    build_user_id: "{{ansible_user_id}}"
        roles:
            -   role: packer/requirements
                become: yes

It is possible to run this on all hosts in images group. They refer to all
``buildhosts`` you have. Use ``run_once: yes`` (if you have just one) or
``serial: 1`` (otherwise) to prevent unexpected side effects (race conditions).

VirtualBox
~~~~~~~~~~

.. _`debian-apt`:  https://gitlab.com/tomaskadlec/ansible-role-debian-apt

The role requires `debian-apt`_ role to be installed and run prior to
installing `VirtualBox`_. It handles adding keys and repositories for apt.

Resulting play might look as follows: ::

-   name: "Install requirements on buildhost"
    hosts: [buildhost]
    vars:
        apt:
            virtualbox: virtualbox-5.1
    pre_tasks:
        -   set_fact:
                build_user_id: "{{ansible_user_id}}"
    roles:
        -   role: debian-apt
            become: yes
        -   role: packer/requirements
            become: yes


.. vim: spelllang=en spell textwidth=80 fo-=l:
.. reformat paragraphs to 80 characters: select using v then hit gq
