ansible-role-packer
===================

.. _`Ansible`: https://ansible.com
.. _`Packer`: https://www.packer.io/   
.. _`KVM`: https://www.linux-kvm.org/page/Main_Page
.. _`QEMU`: http://www.qemu-project.org/
.. _`qemu-kvm`: https://packages.debian.org/search?searchon=names&keywords=qemu-kvm
.. _`genisoimage`: https://packages.debian.org/search?suite=default&section=all&arch=any&searchon=names&keywords=genisoimage   
.. _`7z`: https://packages.debian.org/search?keywords=p7zip-full

The project contains two Ansible_roles that makes use of Packer_ to prepare an
image of a operating system for virtual machines or even physical hosts
(conversion needed but it is simple).

.. _`packer/requirements`: requirements/README.rst
.. _`packer/build`: build/README.rst

* `packer/requirements`_ - install requirements to use Packer_
* `packer/build`_ - builds images

The role takes a bit unusual approach. Each image is considered as a host. It
it must be a member of ``images`` group and has its own configuration in
``host_vars`` folder. However, ``ansible_host`` is set to ``buildhost`` for all
hosts in ``images`` group. That means all tasks are run on ``buildhost`` without
a need to delegate them explicitly. Which host is the ``buildhost`` is
completely up to you. Another advantage of this approach is that there is no
need to loop over complex configurations. Each image-host has its own
configuration separated from the rest.

You may ask why not use Packer directly. There are several reasons:

* Ansible_ is widely known compared to Packer_ in my opinion.
* Ansible_ may be used to provision images and you need to know just one tool.
* Ansible_ is used to install requirements to run Packer_ on ``buildhost``.
* Ansible_ is used to template ``packer.json`` as other files needed, e.g.
  ``preseed.cfg`` for Debian. Thus it allows to reuse the same template for
  multiple images.
* Ansible_ may download ISO images and alter them thus kernel command line
  modifications are much more easier.
* Ansible  

Requirements
------------

* `Packer`_
* `Ansible`_ 2.2 and newer

  Ansible must be configured to merge dicts, e.g. ``hash_behaviour = merge``
  must be present in your ``ansible.cfg``.

* `7z`_ to unpack ISOs
* `genisoimage`_ to create ISOs  
* virtualization platform and several other tools according to selected builder,
  provisioner and postprocessors

  QEMU_ and KVM_ (`qemu-kvm`_ package) are used as default.

The `packer/requirements`_ role may be used to install/upgrade the requirements.
Packer_ can be installed on any Linux host. However, installation of the rest of
the requirements is supported only on Debian GNU/Linux currently.

Installation
------------

There are two possibilities how to install the roles. Choose one that suits you best.

#. Copy the role into the ``roles`` subdirectory: ::

    cd YOUR_PROJECT
    mkdir -p roles/swarm
    wget -O - \
        https://gitlab.com/tomaskadlec/ansible-role-packer/repository/archive.tar.bz2?ref=master \
        | tar xjf - --wildcards -C roles/packer --strip=1

#. If your project uses git you can make use of git submodules: ::

     cd YOUR_PROJECT
     mkdir -p roles
     git submodule add git@gitlab.com:tomaskadlec/ansible-role-packer.git roles/packer

Configuration and usage
-----------------------

Please refer to each role for its configuration and usage.

* `packer/requirements`_ - install requirements to use Packer_
* `packer/build`_ - builds images

.. vim: spelllang=en spell textwidth=80 fo-=l: 
.. reformat paragraphs to 80 characters: select using v then hit gq
